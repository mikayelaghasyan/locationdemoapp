//
//  AppDelegate.swift
//  LocationDemoApp
//
//  Created by Mikayel Aghasyan on 10/20/18.
//  Copyright © 2018 Mikayel Aghasyan. All rights reserved.
//

import UIKit
import GoogleMaps
import SocketIO

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    var socketManager: SocketManager?
    var locationManager = LocationManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        GMSServices.provideAPIKey("AIzaSyDtqLJUHy6tapR-WlHCRJBujlaAwsUCYZs")

        let config: SocketIOClientConfiguration = [.log(false), .compress, .reconnects(true), .reconnectWait(5), .reconnectAttempts(-1)]
        self.socketManager = SocketManager(socketURL: URL(string: "http://34.205.216.212:8000")!, config: config)
        self.socketManager?.defaultSocket.on(clientEvent: .connect, callback: { [weak self] (data, ack) in
            let data = [
                "id": UIDevice.current.identifierForVendor!.uuidString,
                "name": UIDevice.current.name
            ]
            self?.socketManager?.defaultSocket.emit("introduce", with: [data])
        })
        self.socketManager?.defaultSocket.connect()

        locationManager.socket = self.socketManager?.defaultSocket
        if let controller = self.window?.rootViewController as? ViewController {
            controller.socket = self.socketManager?.defaultSocket
            controller.locationManager = locationManager
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

