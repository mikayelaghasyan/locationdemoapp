//
//  ViewController.swift
//  LocationDemoApp
//
//  Created by Mikayel Aghasyan on 10/20/18.
//  Copyright © 2018 Mikayel Aghasyan. All rights reserved.
//

import UIKit
import SocketIO
import GoogleMaps

class ViewController: UIViewController {
    var socket: SocketIOClient?
    var locationManager: LocationManager?

    var observer: Any?

    @IBOutlet weak var mapView: GMSMapView?

    var markers: [String: GMSMarker] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        socket?.on(clientEvent: .connect, callback: { [weak self] (data, ack) in
            self?.socket?.emit("subscribe", with: [])
        })
        socket?.on("locationUpdates", callback: { [weak self] (data, ack) in
            let locations = data.first as? NSArray as? [Any]
            self?.updateMarkers(with: locations)
        })
        if socket?.status == .connected {
            socket?.emit("subscribe", with: [])
        }

        observer = NotificationCenter.default.addObserver(forName: .LocationUpdated, object: nil, queue: nil) { [weak self] (notification) in
            if let location = self?.locationManager?.currentLocation {
                self?.mapView?.animate(to: GMSCameraPosition.camera(withTarget: location, zoom: 14))
                self?.removeObserver()
            }
        }
    }

    deinit {
        socket?.emit("unsubscribe", with: [])
        removeObserver()
    }

    func removeObserver() {
        if let observer = self.observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }

    func updateMarkers(with locations: [Any]?) {
        if let locations = locations {
            locations.forEach { [weak self] (location) in
                guard let location = location as? [String:Any] else { return }
                guard let id = location["id"] as? String else { return }
                guard let coordinateData = location["location"] as? [String: Any] else { return }
                guard let latitude = parseCoordinate(value: coordinateData["lat"]) else { return }
                guard let longitude = parseCoordinate(value: coordinateData["lon"]) else { return }
                let name = location["name"] as? String
                let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                var marker = self?.markers[id]
                if marker == nil {
                    marker = GMSMarker(position: coordinate)
                    marker?.map = self?.mapView
                    marker?.title = name
                    self?.markers[id] = marker
                } else {
                    marker?.position = coordinate
                }
            }
        }
    }

    func parseCoordinate(value: Any?) -> CLLocationDegrees? {
        if let val = value as? String {
            return CLLocationDegrees(val)
        } else if let val = value as? Double {
            return CLLocationDegrees(val)
        } else {
            return nil
        }
    }
}
