//
//  LocationManager.swift
//  LocationDemoApp
//
//  Created by Mikayel Aghasyan on 10/21/18.
//  Copyright © 2018 Mikayel Aghasyan. All rights reserved.
//

import Foundation
import CoreLocation
import SocketIO

class LocationManager: NSObject {
    let locationManager = CLLocationManager()

    let distance: CLLocationDistance = 10

    var socket: SocketIOClient?

    var currentLocation: CLLocationCoordinate2D? {
        didSet {
            if let location = currentLocation {
                NotificationCenter.default.post(name: .LocationUpdated, object: nil)
                let data = [
                    "lat": location.latitude,
                    "lon": location.longitude
                ]
                self.socket?.emit("location", with: [data])
            }
        }
    }
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
    }

    public func startTracking() {
        guard checkAuthorizationStatus() else {
            print("Not authorized to track location")
            return
        }
        guard CLLocationManager.locationServicesEnabled() else {
            print("Location services disabled")
            return
        }
        self.locationManager.startUpdatingLocation()
    }

    fileprivate func checkAuthorizationStatus() -> Bool {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .denied, .notDetermined, .restricted:
            locationManager.requestWhenInUseAuthorization()
            return false
        case .authorizedWhenInUse:
            locationManager.requestAlwaysAuthorization()
            return true
        case .authorizedAlways:
            return true
        }
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.startTracking()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.last?.coordinate
    }
}

extension Notification.Name {
    static let LocationUpdated = NSNotification.Name("LocationUpdated")
}
